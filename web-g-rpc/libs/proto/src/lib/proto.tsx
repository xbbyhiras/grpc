import styles from './proto.module.css';

/* eslint-disable-next-line */
export interface ProtoProps {}

export function Proto(props: ProtoProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Proto!</h1>
    </div>
  );
}

export default Proto;
