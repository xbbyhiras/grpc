import { render } from '@testing-library/react';

import Proto from './proto';

describe('Proto', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Proto />);
    expect(baseElement).toBeTruthy();
  });
});
